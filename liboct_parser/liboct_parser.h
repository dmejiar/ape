/*
 Copyright (C) 2002 M. Marques, A. Castro, A. Rubio, G. Bertsch

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.

*/

#ifndef _LIB_OCT_H
#define _LIB_OCT_H

#include <gsl/gsl_complex.h>

int         oct_parse_init   (char *file_out, int mpiv_node);
int         oct_parse_input  (char *file_in);
void        oct_parse_end    (void);

int         oct_parse_isdef  (char *name);
int         oct_parse_int    (char *name, int def);
double      oct_parse_double (char *name, double def);
gsl_complex oct_parse_complex(char *name, gsl_complex def);
char       *oct_parse_string (char *name, char *def);

/* Now comes stuff for the blocks */
typedef struct oct_parse_sym_block_line{
  int n;
  char **fields;
} oct_parse_sym_block_line;

typedef struct oct_parse_sym_block{
  int n;
  oct_parse_sym_block_line *lines;
} oct_parse_sym_block;

int oct_parse_block        (char *name, oct_parse_sym_block **blk);
int oct_parse_block_end    (oct_parse_sym_block **blk);
int oct_parse_block_n      (oct_parse_sym_block *blk);
int oct_parse_block_cols   (oct_parse_sym_block *blk, int l);
int oct_parse_block_int    (oct_parse_sym_block *blk, int l, int col, int *r);
int oct_parse_block_double (oct_parse_sym_block *blk, int l, int col, double *r);
int oct_parse_block_complex(oct_parse_sym_block *blk, int l, int col, gsl_complex *r);
int oct_parse_block_string (oct_parse_sym_block *blk, int l, int col, char **r);

/* from parse_exp.c */
enum oct_parse_pr_type {OCT_PARSE_PR_NONE, OCT_PARSE_PR_CMPLX, OCT_PARSE_PR_STR};
typedef struct oct_parse_result{
  union {
    gsl_complex c;
    char *s;
  } value;
  enum oct_parse_pr_type type;
} oct_parse_result;

void oct_parse_result_free(oct_parse_result *t);

int oct_parse_exp(char *exp, oct_parse_result *t);

void oct_parse_putsym_int(char *s, int i);
void oct_parse_putsym_double(char *s, double d);
void oct_parse_putsym_complex(char *s, gsl_complex c);

#endif
