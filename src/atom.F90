!! Copyright (C) 2004-2015 M. Oliveira, F. Nogueira
!! Copyright (C) 2011-2012 T. Cerqueira
!! Copyright (C) 2014 P. Borlido
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module atom_m
  use global_m
  use oct_parser_m
  use io_m
  use iso_c_binding
  use messages_m
  use units_m
  use output_m
  use mesh_m
  use mixing_m
  use ps_io_m
  use quantum_numbers_m
  use potentials_m
  use wave_equations_m
  use eigensolver_m
  use states_m
  use states_batch_m
  use hartree_m
  use xc_m
  use hamann_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
    module procedure atom_copy
  end interface


                    !---Derived Data Types---!

  type atom_t
    private
    integer  :: type
    real(R8) :: z
    character(3) :: symbol
    integer   :: wave_eq
    integer   :: theory_level
    type(xc_t):: xc_model
    type(potential_t) :: potential
    type(mesh_t) :: m
    integer :: nspin
    integer :: smearing
    type(states_batch_t) :: states
    type(integrator_t) :: integrator_sp
    type(integrator_t) :: integrator_dp
  end type atom_t


                    !---Global Variables---!

  integer, parameter :: TEST_LD = 1, &
                        TEST_DM = 2, &
                        TEST_IP = 4, &
                        TEST_EE = 8

  integer, parameter :: ATOM_AE = 1, &
                        ATOM_PS = 2, &
                        ATOM_KB = 3


                    !---Public/Private Statements---!

  private
  public :: atom_t, &
            atom_null, &
            atom_create_ae, &
            atom_create_ps, &
            atom_create_kb, &
            atom_save, &
            atom_load, &
            atom_end, &
            atom_xc_eval, &
            atom_solve, &
            atom_test, &
            atom_ip, &
            atom_output, &
            TEST_LD, TEST_DM, TEST_IP, TEST_EE

contains

  !-----------------------------------------------------------------------
  !> Nullifies and sets to zero all the components of the atom atm.       
  !-----------------------------------------------------------------------
  subroutine atom_null(atm)
    type(atom_t), intent(out) :: atm

    atm%type = 0
    atm%z = M_ZERO
    atm%symbol = ""
    atm%wave_eq = 0
    atm%theory_level = 0
    atm%nspin = 0
    atm%smearing = 0
    call xc_null(atm%xc_model)
    call potential_null(atm%potential)
    call mesh_null(atm%m)
    call integrator_null(atm%integrator_sp)
    call integrator_null(atm%integrator_dp)
    call states_batch_null(atm%states)

  end subroutine atom_null

  !-----------------------------------------------------------------------
  !> Initializes the atom by reading the relevant input options and prints 
  !> some relevant informantion.                                           
  !-----------------------------------------------------------------------
  subroutine atom_init_states_from_block(atm, block_name)
    type(atom_t),     intent(inout) :: atm
    character(len=*), intent(in)    :: block_name
    
    type input_data
      integer  :: n
      integer  :: l
      real(R8) :: occ(2)
      real(R8) :: m
    end type input_data

    integer :: n_lines, n_id, n_rare, l1, i, k, n_cols, ierr, ij, is, im
    type(c_ptr) :: blk
    real(R8) :: occ
    character(2) :: rare
    type(qn_t) :: qn
    type(input_data), allocatable :: id(:)
    type(input_data), parameter :: ID_RARE(15) = &
         (/ input_data(1, 0, (/M_ONE,  M_ONE/),   M_ZERO), &
            input_data(2, 0, (/M_ONE,  M_ONE/),   M_ZERO), &
            input_data(2, 1, (/M_THREE,M_THREE/), M_ZERO), &
            input_data(3, 0, (/M_ONE,  M_ONE/),   M_ZERO), &
            input_data(3, 1, (/M_THREE,M_THREE/), M_ZERO), &
            input_data(3, 2, (/M_FIVE, M_FIVE/),  M_ZERO), &
            input_data(4, 0, (/M_ONE,  M_ONE/),   M_ZERO), &
            input_data(4, 1, (/M_THREE,M_THREE/), M_ZERO), &
            input_data(4, 2, (/M_FIVE, M_FIVE/),  M_ZERO), &
            input_data(5, 0, (/M_ONE,  M_ONE/),   M_ZERO), &
            input_data(5, 1, (/M_THREE,M_THREE/), M_ZERO), &
            input_data(4, 3, (/M_SEVEN,M_SEVEN/), M_ZERO), &
            input_data(5, 2, (/M_FIVE, M_FIVE/),  M_ZERO), &
            input_data(6, 0, (/M_ONE,  M_ONE/),   M_ZERO), &
            input_data(6, 1, (/M_THREE,M_THREE/), M_ZERO) /)

    call push_sub("atom_init_states_from_block")

    !Open the orbitals block and get the number of lines
    ierr = oct_parse_f90_block(trim(block_name), blk)
    if (ierr /= 0) then
      write(message(1),'("Unable to open ",A," block.")') trim(block_name)
      call write_fatal(1)
    end if
    n_lines = oct_parse_f90_block_n(blk)
    if (n_lines < 1) then
      write(message(1),'("No ",A," block specified on input.")') trim(block_name)
      call write_fatal(1)
    end if

    !Do we have a rare gas in the orbitals block?
    if (oct_parse_f90_block_cols(blk, 0) == 1) then
      ierr = oct_parse_f90_block_string(blk, 0, 0, rare)
      if (ierr /= 0) then
        write(message(1),'("Unable to read a string from line 1 and column 1 of the ",A," block.")') trim(block_name)
        call write_fatal(1)
      end if
      select case (rare)
      case ("He") 
        n_rare = 1
      case ("Ne") 
        n_rare = 3
      case ("Ar") 
        n_rare = 5
      case ("Kr") 
        n_rare = 8
      case ("Xe") 
        n_rare = 11
      case ("Rn") 
        n_rare = 15
      case default
        message(1) = "Wrong input for orbitals block: unknown rare gas."
        call write_fatal(1)
      end select
      l1 = 1
      n_id = n_rare + n_lines - 1
    else
      n_rare = 0
      l1 = 0
      n_id = n_lines
    end if

    allocate(id(n_id))
    id = input_data(0, 0, (/M_ZERO,M_ZERO/), M_ZERO)
    id(1:n_rare) = ID_RARE(1:n_rare)

    k = n_rare
    do i = l1, n_lines - 1
      k = k + 1

      !Read input
      n_cols = oct_parse_f90_block_cols(blk, i)
      ierr = oct_parse_f90_block_int(blk, i, 0, id(k)%n)
      if (ierr /= 0) then
        write(message(1),'("Unable to read an integer from line ",I2," and column 1 of the ",A," block.")') i+1, trim(block_name)
        call write_fatal(1)
      end if
      ierr = oct_parse_f90_block_int(blk, i, 1, id(k)%l)
      if (ierr /= 0) then
        write(message(1),'("Unable to read an integer from line ",I2," and column 2 of the ",A," block.")') i+1, trim(block_name)
        call write_fatal(1)
      end if
      id(k)%m = M_ZERO
      select case (n_cols)
      case (3)
        ierr = oct_parse_f90_block_double(blk, i, 2, id(k)%occ(1))
        if (ierr /= 0) then
          write(message(1),'("Unable to read a double from line ",I2," and column 3 of the ",A," block.")') i+1, trim(block_name)
          call write_fatal(1)
        end if
        if (atm%nspin == 2) id(k)%occ = id(k)%occ(1)/M_TWO
      case (4)
        if (atm%nspin == 2 .and. atm%wave_eq == DIRAC) then
          ierr = oct_parse_f90_block_double(blk, i, 2, id(k)%m)
          if (ierr /= 0) then
            write(message(1),'("Unable to read a double from line ",I2," and column 3 of the ",A," block.")') i+1, trim(block_name)
            call write_fatal(1)
          end if
          ierr = oct_parse_f90_block_double(blk, i, 3, id(k)%occ(1))
          if (ierr /= 0) then
            write(message(1),'("Unable to read a double from line ",I2," and column 4 of the ",A," block.")') i+1, trim(block_name)
            call write_fatal(1)
          end if
          if (abs(id(k)%m) == id(k)%l + M_HALF) then
            id(k)%occ(2) = M_ZERO
          else
            id(k)%occ =  id(k)%occ(1)/M_TWO
          end if
        else
          ierr = oct_parse_f90_block_double(blk, i, 2, id(k)%occ(1))
          if (ierr /= 0) then
            write(message(1),'("Unable to read a double from line ",I2," and column 3 of the ",A," block.")') i+1, trim(block_name)
            call write_fatal(1)
          end if
          ierr = oct_parse_f90_block_double(blk, i, 3, id(k)%occ(2))
          if (ierr /= 0) then
            write(message(1),'("Unable to read a double from line ",I2," and column 4 of the ",A," block.")') i+1, trim(block_name)
            call write_fatal(1)
          end if
        end if
      case (5)
        if (atm%wave_eq /= DIRAC .or. atm%nspin /= 2) then
          write(message(1),'("There is something wrong at line ",I2," of the Orbitals block.")') i+1
          message(2) = "Illegal number of columns for an unRel unPol calc."
          call write_fatal(2)
        end if
        ierr = oct_parse_f90_block_double(blk, i, 2, id(k)%m)
        if (ierr /= 0) then
          write(message(1),'("Unable to read a double from line ",I2," and column 3 of the ",A," block.")') i+1, trim(block_name)
          call write_fatal(1)
        end if
        if (abs(id(k)%m) == id(k)%l + M_HALF) then
          write(message(1),'("There is something wrong at line ",I2," of the Orbitals block.")') i+1
          message(2) = "Orbitals with 2|m| = 2l + 1 cannot have two occupancies specified."
          call write_fatal(2)
        end if
        ierr = oct_parse_f90_block_double(blk, i, 3, id(k)%occ(1))
        if (ierr /= 0) then
          write(message(1),'("Unable to read a double from line ",I2," and column 4 of the ",A," block.")') i+1, trim(block_name)
          call write_fatal(1)
        end if
        ierr = oct_parse_f90_block_double(blk, i, 4, id(k)%occ(2))
        if (ierr /= 0) then
          write(message(1),'("Unable to read a double from line ",I2," and column 5 of the ",A," block.")') i+1, trim(block_name)
          call write_fatal(1)
        end if
      case default
        write(message(1),'("There is something wrong at line ",I2," of the Orbitals block.")') i+1
        message(2) = "Illegal number of columns."
        call write_fatal(2)        
      end select

      !Check input
      if (id(k)%n < 1) then
        write(message(1),'("There is something wrong at line ",I2," of the Orbitals block.")') i+1
        message(2) = "Illegal main quantum number."
        call write_fatal(2)
      else if (id(k)%l < 0 .or. id(k)%l > id(k)%n - 1) then
        write(message(1),'("There is something wrong at line ",I2," of the Orbitals block.")') i+1
        message(2) = "Illegal angular momentum quantum number."
        call write_fatal(2)
      else if (  ((sum(id(k)%occ)) > (id(k)%l*M_FOUR + M_TWO) ) .or. &
            (any(id(k)%occ > (id(k)%l*M_FOUR + M_TWO)/atm%nspin)) .or. &
            (id(k)%m /= M_ZERO .and. any(id(k)%occ > M_ONE))) then
        write(message(1),'("There is something wrong at line ",I2," of the Orbitals block")') i+1
        message(2) = "Illegal occupancies."
        call write_fatal(2)
      end if

    end do

    !Close the Orbitals block
    call oct_parse_f90_block_end(blk)

    !Check that there are no duplicated lines
    do i = 1, n_id - 1
      do k = i + 1, n_id
        if (id(i)%n == id(k)%n .and. id(i)%l == id(k)%l .and. id(i)%m == id(k)%m ) then
          message(1) = "There are at least two lines with the same set of quantum numbers."
          message(2) = "in the Orbitals block."
          call write_fatal(2)
        end if
      end do
    end do
 
    !Initialize states
    call states_batch_null(atm%states)
    do i = 1, n_id
      select case (atm%wave_eq)
      case (SCHRODINGER, SCALAR_REL)
        if (atm%nspin == 1) then
          qn = qn_init(id(i)%n, id(i)%l, M_ZERO)
          call create_state(atm%states, qn, sum(id(i)%occ))
        else
          do is = 1, 2
            qn = qn_init(id(i)%n, id(i)%l, is - M_THREE/M_TWO)
            call create_state(atm%states, qn, id(i)%occ(is))
          end do
        end if

      case (DIRAC)
        if (atm%nspin == 1) then
          if (id(i)%l == 0) then
            qn = qn_init(id(i)%n, id(i)%l, M_ZERO, j=M_HALF)
            call create_state(atm%states, qn, sum(id(i)%occ))
          else
            do ij = 1, 2
              qn = qn_init(id(i)%n, id(i)%l, M_ZERO, j=id(i)%l+ij-M_THREE/M_TWO)
              occ = sum(id(i)%occ)*(M_TWO*qn%j + M_ONE)/(M_FOUR*qn%l + M_TWO)
              call create_state(atm%states, qn, occ)
            end do
          end if        

        else
          if (id(i)%m == M_ZERO) then
            occ = sum(id(i)%occ)/(M_FOUR*id(i)%l + M_TWO)

            qn = qn_init(id(i)%n, id(i)%l, M_ZERO, m= (id(i)%l+M_HALF), sg=M_ZERO)
            call create_state(atm%states, qn, occ)
            qn = qn_init(id(i)%n, id(i)%l, M_ZERO, m=-(id(i)%l+M_HALF), sg=M_ZERO)
            call create_state(atm%states, qn, occ)

            do im = 1, 2*id(i)%l
              do is = 1, 2
                qn = qn_init(id(i)%n, id(i)%l, M_ZERO, m=im-id(i)%l-M_HALF, sg=M_THREE/M_TWO-is)
                call create_state(atm%states, qn, occ)
              end do
            end do
          else
            if (abs(id(i)%m) == id(i)%l + M_HALF) then
              qn = qn_init(id(i)%n, id(i)%l, M_ZERO, m=id(i)%m, sg=M_ZERO)
              call create_state(atm%states, qn, sum(id(i)%occ))
            else
              do is = 1, 2
                qn = qn_init(id(i)%n, id(i)%l, M_ZERO, m=id(i)%m, sg=M_THREE/M_TWO-is)
                call create_state(atm%states, qn, id(i)%occ(is))
              end do
            end if
          end if
        end if

      end select
    end do
    deallocate(id)

    call pop_sub()
  contains

    subroutine create_state(batch, qn, occ)
      type(states_batch_t), intent(inout) :: batch
      type(qn_t),           intent(inout) :: qn
      real(R8),             intent(in)    :: occ

      character(10) :: label
      type(state_t), pointer :: state

      !Get label and correct quantum numbers
      label = qn_label(qn, full=.false.)
      if (atm%type == ATOM_PS) then
        qn%n = qn%n - &
             minval(id(:)%n, mask=id(:)%l == qn%l) + qn%l + 1
      end if

      !Allocate memory, init state, and add it to the batch
      allocate(state)
      call state_null(state)
      call state_init(state, atm%m, qn, occ, label)
      call states_batch_add(batch, state)

      !We do not deallocate the state, because the batch has a pointer to that chunk of memory
      nullify(state)

    end subroutine create_state

  end subroutine atom_init_states_from_block

  !-----------------------------------------------------------------------
  !> Writes the atom information to the "dir/data" file.                   
  !-----------------------------------------------------------------------
  subroutine atom_save(atm, dir)
    type(atom_t),     intent(in) :: atm
    character(len=*), intent(in) :: dir

    integer :: unit, i

    call push_sub("atom_save")

    call io_open(unit, trim(dir)//"/data", form='unformatted')

    write(unit) atm%type, atm%z, atm%symbol, atm%theory_level, atm%wave_eq, atm%nspin, atm%smearing

    !Write exchange-correlation model
    call xc_model_save(unit, atm%xc_model)

    !Write mesh
    call mesh_save(unit, atm%m)

    !Write states info
    write(unit) states_batch_size(atm%states)
    do i = 1, states_batch_size(atm%states)
      call state_save(unit, states_batch_get(atm%states,i))
    end do

    !Write potential
    call potential_save(unit, atm%potential)

    close(unit)

    call pop_sub()
  end subroutine atom_save

  !-----------------------------------------------------------------------
  !> Reads the atom information from the "dir/data" file.                  
  !-----------------------------------------------------------------------
  subroutine atom_load(atm, dir)
    type(atom_t),     intent(inout) :: atm
    character(len=*), intent(in) :: dir

    integer :: unit, i, n_states
    type(state_t), pointer :: state

    call push_sub("atom_load")

    !Open file
    call io_open(unit, trim(dir)//"/data", form='unformatted', status='old')

    !
    read(unit) atm%type, atm%z, atm%symbol, atm%theory_level, atm%wave_eq, atm%nspin, atm%smearing

    !Read exchange-correlation model
    call xc_model_load(unit, atm%xc_model)

    !Read mesh
    call mesh_load(unit, atm%m)

    !Read states data
    read(unit) n_states
    do i = 1, n_states
      allocate(state)
      call state_null(state)
      call state_load(unit, state)
      call states_batch_add(atm%states, state)
      nullify(state)
    end do

    !Read potential
    call potential_load(unit, atm%potential)

    !Initialize integrator
    call integrator_init(atm%integrator_sp, atm%integrator_dp)

    close(unit)

    call pop_sub()
  end subroutine atom_load

  !-----------------------------------------------------------------------
  !> Frees all the memory associated to atm.                               
  !-----------------------------------------------------------------------
  subroutine atom_end(atm)
    type(atom_t), intent(inout) :: atm

    integer :: i
    type(state_t), pointer :: state

    call push_sub("atom_end")

    !End states
    do i = 1, states_batch_size(atm%states)
      state => states_batch_get(atm%states, i)
      call state_end(state)
      deallocate(state)
    end do
    call states_batch_end(atm%states)

    !End potential
    call potential_end(atm%potential)

    !End xc
    call xc_end(atm%xc_model)

    !End mesh
    call mesh_end(atm%m)

    !End integrators
    call integrator_end(atm%integrator_sp)
    call integrator_end(atm%integrator_dp)

    !Set everything to zero
    atm%type = 0
    atm%z = M_ZERO
    atm%symbol = ""
    atm%theory_level = 0
    atm%wave_eq = 0
    atm%nspin = 0
    atm%smearing = 0

    call pop_sub()
  end subroutine atom_end

  !-----------------------------------------------------------------------
  !> Copies atom_b to atom_a.                                              
  !-----------------------------------------------------------------------
  subroutine atom_copy(atm_a, atm_b)
    type(atom_t), intent(inout) :: atm_a
    type(atom_t), intent(in)    :: atm_b

    integer :: i
    type(state_t), pointer :: state

    call push_sub("atom_copy")

    call atom_end(atm_a)

    atm_a%type          = atm_b%type
    atm_a%z             = atm_b%z
    atm_a%symbol        = atm_b%symbol
    atm_a%wave_eq       = atm_b%wave_eq
    atm_a%theory_level  = atm_b%theory_level
    atm_a%xc_model      = atm_b%xc_model
    atm_a%potential     = atm_b%potential
    atm_a%m             = atm_b%m
    atm_a%nspin         = atm_b%nspin
    atm_a%smearing      = atm_b%smearing
    do i = 1, states_batch_size(atm_b%states)
      allocate(state)
      call state_null(state)
      state = states_batch_get(atm_b%states, i)
      call states_batch_add(atm_a%states, state)
      nullify(state)
    end do
    atm_a%integrator_sp = atm_b%integrator_sp
    atm_a%integrator_dp = atm_b%integrator_dp

    call pop_sub()
  end subroutine atom_copy

  !-----------------------------------------------------------------------
  !>
  !-----------------------------------------------------------------------
  subroutine atom_eigensolve(atm, eigensolver)
    type(atom_t),        intent(inout) :: atm
    type(eigensolver_t), intent(in)    :: eigensolver

    logical :: polarized
    integer :: i, n_folds
    type(states_batch_t), allocatable :: folds(:)

    call push_sub("atom_eigensolve")

    !Split states into batches
    polarized = potential_is_polarized(atm%potential)
    n_folds = states_batch_number_of_folds(atm%states, atm%theory_level, polarized)
    allocate(folds(n_folds))
    do i = 1, n_folds
      call states_batch_null(folds(i))
    end do
    call states_batch_split_folds(atm%states, folds, atm%theory_level, polarized)

    !Solve the one-electron equations
    do i = 1, n_folds
      call states_batch_eigensolve(folds(i), atm%m, atm%wave_eq, atm%potential, &
                                     atm%integrator_dp, atm%integrator_sp, eigensolver)
    end do

    !End batches
    do i = 1, n_folds
      call states_batch_end(folds(i))
    end do

    call pop_sub()
  end subroutine atom_eigensolve

  !-----------------------------------------------------------------------
  !> Solve the Kohn-Sham equations for the atom in a self-consitent way an 
  !> outputs the relevant results.                                         
  !-----------------------------------------------------------------------
  subroutine atom_solve(atm, eigensolver, unit)
    type(atom_t),        intent(inout) :: atm         !< atom object
    type(eigensolver_t), intent(in)    :: eigensolver !< information about the eigensolver
    integer, optional,   intent(in)    :: unit        !< output file

    integer  :: nitmax, nit, p, order, mix_size
    real(R8) :: rel_etot, abs_etot, rel_dens, abs_dens
    real(R8) :: conv_rel_etot, conv_abs_etot, conv_rel_dens, conv_abs_dens
    real(R8) :: etot_in, etot_out
    real(R8), allocatable :: density_out(:,:), density_in(:,:)
    type(mixing_t) :: mix
    type(potential_t)  :: potential_out

    call push_sub("atom_solve")

    message(1) = ""
    message(2) = "Starting SCF process"
    call write_info(2)

    !Read convergence parameters
    conv_rel_etot = oct_parse_f90_double('ConvRelEnergy', M_ZERO)
    conv_abs_etot = oct_parse_f90_double('ConvAbsEnergy', M_ZERO)
    conv_rel_dens = oct_parse_f90_double('ConvRelDens',   1e-8_R8)
    conv_abs_dens = oct_parse_f90_double('ConvAbsDens',   M_ZERO)
    if (conv_rel_etot < M_ZERO .and. conv_abs_etot < M_ZERO .and. &
        conv_rel_dens < M_ZERO .and. conv_abs_dens < M_ZERO) then
      message(1) = "Not all the convergence criteria can be zero."
      call write_fatal(1)
    end if
    message(1) = "  Convergence tolerance: ConvAbsDens ConvRelDens"
    write(message(2),'(25X,ES10.3E2,2X,ES10.3E2)') conv_abs_dens, conv_rel_dens
    message(3) = "                         ConvAbsEnergy ConvRelEnergy"
    write(message(4),'(25X,ES10.3E2,2X,ES10.3E2)') conv_abs_etot, conv_rel_etot
    call write_info(4,20)

    !Read maximum number of iterations during the SCF procedure
    nitmax = oct_parse_f90_int('MaximumIter', 300)
    if (nitmax < 0) then
      message(1) = "MaximumIter can''t take negative values."
      call write_fatal(1)
    end if
    write(message(1),'(2X,"Maximum number of iterations: ",I6)') nitmax
    call write_info(1,20)

    !Initialize mixing
    call mixing_null(mix)
    select case (atm%theory_level)
    case (INDEPENDENT_PARTICLES, DFT)
      mix_size = atm%m%np*atm%nspin*2
    case (HARTREE_FOCK, HYBRID)
      mix_size = 2*atm%m%np*states_batch_size(atm%states)
    end select
    call mixing_init(mix, mix_size)

    !Nullify potentials
    call potential_null(potential_out)

    !Initialize some stuff
    allocate(density_out(atm%m%np, atm%nspin), density_in(atm%m%np, atm%nspin))
    call atom_energies(atm, etot_in)
    potential_out = atm%potential
    density_in = states_batch_density(atm%states, atm%nspin, atm%m)


                    !---Self Consistent Field cycle---!


    message(1) = ""
    message(2) = "Performing SCF Cycle"
    call write_info(2)

    !Main loop
    do nit = 1, nitmax
      if (in_debug_mode) then
        call atom_output(atm, "debug_info")
      end if

      !Get states eigenvalues and wavefunctions
      call atom_eigensolve(atm, eigensolver)

      !Get new occupancies
      call states_batch_smearing(atm%states, atm%smearing, eigensolver%tol)


      !Get the output density, the output potential and the output energy
      density_out = states_batch_density(atm%states, atm%nspin, atm%m)
      call atom_update_potential(atm, potential_out)
      call atom_energies(atm, etot=etot_out,potential=potential_out)

      !Calculate the convergence for the density and for the total energy
      abs_dens = M_ZERO
      do p = 1, atm%nspin
        abs_dens = abs_dens + mesh_integrate(atm%m, abs(density_in(:,p) &
                                             - density_out(:,p)))
      end do
      abs_dens = abs_dens
      rel_dens = abs_dens/states_batch_charge(atm%states)
      abs_etot = abs(etot_out - etot_in)
      rel_etot = abs((etot_out - etot_in)/etot_out)

      !Print intermediate results
      message(1) = ""
      write(message(2),'(2X,"Iteration number: ",I6)') nit
      write(message(3),'(4X,"Total energy: ",F14.6," ",A)') &
                            etot_out/units_out%energy%factor, trim(units_out%energy%abbrev)
      write(message(4),'(4X,"ConvAbsDens =",ES9.2E2,1X,"ConvAbsEnergy =",ES9.2E2)') &
                                                 abs_dens, abs_etot/units_out%energy%factor
      write(message(5),'(4X,"ConvRelDens =",ES9.2E2,1X,"ConvRelEnergy =",ES9.2E2)') &
                                                                         rel_dens, rel_etot
      call write_info(5,30)
      call states_batch_sort(atm%states, SORT_EV)
      call states_batch_output_eigenvalues(atm%states, atm%nspin, verbose_limit=30)

      !Check for convergence
      if ( (abs_etot <= conv_abs_etot .or. conv_abs_etot == M_ZERO) .and. &
           (rel_etot <= conv_rel_etot .or. conv_rel_etot == M_ZERO) .and. &
           (abs_dens <= conv_abs_dens .or. conv_abs_dens == M_ZERO) .and. &
           (rel_dens <= conv_rel_dens .or. conv_rel_dens == M_ZERO) .or.  &
            states_batch_charge(atm%states) == M_ZERO                       ) exit

      !See if the maximum number of iterations as been reached
      if (nit == nitmax) then
        message(1) = "The SCF procedure did not converge"
        call write_warning(1)
      end if

      !Mix input and output potentials
      call potential_mix(atm%potential, potential_out, nit, mix)

      !Save values for next iterations
      etot_in = etot_out
      density_in = density_out
    end do


                   !---Final steps---!

    !Get the eigenvalues and wavefunctions of all the states
    call atom_eigensolve(atm, eigensolver)
    call states_batch_smearing(atm%states, atm%smearing, eigensolver%tol)

    !Get the potential
    call atom_update_potential(atm)

    !Deallocate memory
    deallocate(density_out, density_in)
    call potential_end(potential_out)
    call mixing_end(mix)

    !Output results and information about the SCF cycle to the output
    message(1) = ""
    message(2) = "  Final results for SCF procedure:"
    call write_info(2,20)
    call states_batch_sort(atm%states, SORT_EV)
    call states_batch_output_eigenvalues(atm%states, atm%nspin, verbose_limit=30)
    
    if (present(unit)) then
      write(unit,*)
      write(unit,'("SCF cycle information:")') 
      write(unit,'(2X,"Number of iterations: ",I6)') nit
      write(unit,'(2X,"Convergence: ")')
      write(unit,'(4X,"ConvAbsDens   =",ES9.2E2," (",ES9.2E2,")")') abs_dens, conv_abs_dens
      write(unit,'(4X,"ConvRelDens   =",ES9.2E2," (",ES9.2E2,")")') rel_dens, conv_rel_dens
      write(unit,'(4X,"ConvAbsEnergy =",ES9.2E2," (",ES9.2E2,")")') &
           abs_etot/units_out%energy%factor, conv_abs_etot/units_out%energy%factor
      write(unit,'(4X,"ConvRelEnergy =",ES9.2E2," (",ES9.2E2,")")') rel_etot, conv_rel_etot
      if (nit > nitmax) then
        write(unit,'(4X,"The SCF procedure did not converge")')
      end if
      if (atm%nspin == 2) then
        write(unit,*)
        write(unit,'("Total Magnetic Moment: ",F12.4)') mesh_integrate(atm%m, &
             states_batch_magnetization_density(atm%states, atm%m))*M_FOUR*M_PI
      end if

      !Output energies and density moments to file
      write(unit,*)
      write(unit,'("Density Moments")')
      do order = -2, 5
        if (order == 0) cycle
        write(unit,'("  <r^",I2,"> = ",F12.3)') order, & 
             states_batch_density_moment(atm%states, atm%m, order)/&
             units_out%length%factor**order
      end do
      write(unit,*)
      call atom_output_energies(atm, unit)
      write(unit,*)
      call states_batch_output_eigenvalues(atm%states, atm%nspin, unit)
    end if

    call pop_sub()
  end subroutine atom_solve

  !-------------------------------------------------------------------------
  !> Determines the ionization potential for a given atom.                   
  !-------------------------------------------------------------------------
  subroutine atom_ip(atm,  eigensolver)
    type(atom_t),        intent(inout) :: atm
    type(eigensolver_t), intent(in)    :: eigensolver
    
    real(R8) :: ip_homo, ip_ediff, e1, e2, ip(atm%nspin)

    call push_sub("atom_ip")

    !First we get the ionization potential from the HOMO eigenvalue
    ip = states_batch_ip(atm%states, atm%nspin)
    ip_homo = minval(ip, ip /= M_ZERO)

    !Next we get the ionization potential from the total energy difference
    call atom_energies(atm, e1)
    call states_batch_smearing(atm%states, atm%smearing, M_ZERO, new_charge=states_batch_charge(atm%states) - M_ONE)
    call atom_update_potential(atm)
    call atom_solve(atm, eigensolver, unit=info_unit("ip"))
    call atom_energies(atm, e2)
    ip_ediff = e2 - e1

    !Write results
    message(1) = ""
    message(2) = ""
    write(message(3),'("Ionization potential [",A,"]")') trim(units_out%energy%abbrev)
    write(message(4),'("  From HOMO Eigenvalue:         ",F14.6)') ip_homo/units_out%energy%factor
    write(message(5),'("  From Total Energy Difference: ",F14.6)') ip_ediff/units_out%energy%factor
    call write_info(5)
    call write_info(5, unit = info_unit("ip"))

    call pop_sub()
  end subroutine atom_ip

  !-----------------------------------------------------------------------
  !> Computes the various terms of the energy.                              
  !-----------------------------------------------------------------------
  subroutine atom_energies(atm, etot, ekin, eh, eext, exc, potential)
    type(atom_t),                intent(inout) :: atm       !< atom information
    real(R8),                    intent(out)   :: etot      !< total enegy
    real(R8),          optional, intent(out)   :: ekin      !< kinetic energy
    real(R8),          optional, intent(out)   :: eh        !< electron-electron interaction energy
    real(R8),          optional, intent(out)   :: eext      !< electron-nucleus/ion interaction energy
    real(R8),          optional, intent(out)   :: exc       !< exchange-correlation energy
    type(potential_t), optional, intent(in)    :: potential !< potential to be used when calculating kinetic energy

    integer :: i
    real(R8) :: ekin_, eh_, eext_, exc_, exx
    real(R8), allocatable :: density(:)

    call push_sub("atom_energies")

    !Kinetic
    ekin_ = M_ZERO
    do i = 1, states_batch_size(atm%states)
      if (present(potential)) then
        ekin_ = ekin_ + state_kinetic_energy(atm%m, states_batch_get(atm%states, i), potential)
      else
        ekin_ = ekin_ + state_kinetic_energy(atm%m, states_batch_get(atm%states, i), atm%potential)
      end if
    end do

    !External (either electron-nucleus or electron-ion)
    eext_ = M_ZERO
    do i = 1, states_batch_size(atm%states)
      if (present(potential)) then
        eext_ = eext_ + state_external_energy(atm%m, states_batch_get(atm%states, i), potential)
      else
        eext_ = eext_ + state_external_energy(atm%m, states_batch_get(atm%states, i), atm%potential)
      end if
    end do

    !Hartree and exchange-correlation
    allocate(density(atm%m%np))
    density = states_batch_charge_density(atm%states, atm%m)
    select case (atm%theory_level)
    case (INDEPENDENT_PARTICLES)
      eh_ = M_ZERO; exc_ = M_ZERO
    case (DFT)
      call hartree_potential(atm%m, density, states_batch_charge(atm%states), eh = eh_)
      call xc_potential(atm%xc_model, atm%m, atm%states, atm%nspin, exc = exc_) 
    case (HARTREE_FOCK)
      call hartree_potential(atm%m, density, states_batch_charge(atm%states), eh = eh_)
      call states_batch_exchange_energy(atm%m, atm%states, exc_)
    case (HYBRID)
      call hartree_potential(atm%m, density, states_batch_charge(atm%states), eh = eh_)
      call states_batch_exchange_energy(atm%m, atm%states, exx)
      call xc_potential(atm%xc_model, atm%m, atm%states, atm%nspin, exc = exc_) 
      exc_ = exc_ + exx*xc_exx_mixing(atm%xc_model)
    end select

    !Total energy
    etot = ekin_ + eext_ + eh_ + exc_

    if (present(ekin)) ekin = ekin_
    if (present(eext)) eext = eext_
    if (present(eh))   eh   = eh_
    if (present(exc))  exc  = exc_

    deallocate(density)

    call pop_sub()
  end subroutine atom_energies

  !-----------------------------------------------------------------------
  !> Writes the atomic energies either to a file or to the screen in a     
  !> nice readable format.                                                 
  !-----------------------------------------------------------------------
  subroutine atom_output_energies(atm, unit, verbose_limit)
    type(atom_t),           intent(inout) :: atm           !< atom information
    integer,      optional, intent(in)    :: unit          !< unit where to write the output
    integer,      optional, intent(in)    :: verbose_limit !< the verbosity level of the current run

    real(R8) :: etot, ekin, eh, eext, exc

    call push_sub("atom_output_energies")

    call atom_energies(atm, etot, ekin, eh, eext, exc)

    write(message(1),'("Energies [",A,"]")') trim(units_out%energy%abbrev)
    write(message(2),'("  Total energy:                         ",F14.6)') etot/units_out%energy%factor
    write(message(3),'("  Kinetic energy:                       ",F14.6)') ekin/units_out%energy%factor
    write(message(4),'("  Electron-electron interaction energy: ",F14.6)') eh/units_out%energy%factor
    write(message(5),'("  Electron-nucleus interaction energy:  ",F14.6)') eext/units_out%energy%factor
    write(message(6),'("  Exchange-correlation energy:          ",F14.6)') exc/units_out%energy%factor
    call write_info(6, verbose_limit=verbose_limit, unit=unit)

    call pop_sub() 
  end subroutine atom_output_energies

  !-----------------------------------------------------------------------
  !> Updates the potential.                                                
  !-----------------------------------------------------------------------
  subroutine atom_update_potential(atm, potential)
    type(atom_t),                intent(inout) :: atm       !< atom information
    type(potential_t), optional, intent(out)   :: potential !< potential to update. If not present, use the one of the atom.
    
    real(R8), allocatable :: vh(:,:), vxc(:,:), vxctau(:,:), v_hf(:,:), n_hf(:,:) 
    type(qn_t), allocatable :: qn(:)

    call push_sub("atom_update_potential")

    select case (atm%theory_level)
    case (INDEPENDENT_PARTICLES, DFT)
      allocate(vh(atm%m%np, atm%nspin), vxc(atm%m%np, atm%nspin), vxctau(atm%m%np, atm%nspin))

      !Compute new potential
      if (atm%theory_level == INDEPENDENT_PARTICLES) then
        vh = M_ZERO; vxc = M_ZERO; vxctau = M_ZERO
      else
        call hartree_potential(atm%m,  states_batch_charge_density(atm%states, atm%m), &
                              states_batch_charge(atm%states), vh)
        call xc_potential(atm%xc_model, atm%m, atm%states, atm%nspin, vxc=vxc, vxctau=vxctau)
      end if

      !Update potential
      if (present(potential)) then
        call potential_update_vhxc(potential, vh + vxc, vxctau)
      else        
        call potential_update_vhxc(atm%potential, vh + vxc, vxctau)
      end if

      deallocate(vh, vxc, vxctau)

    case (HARTREE_FOCK, HYBRID)
      allocate(v_hf(atm%m%np, states_batch_size(atm%states)))
      allocate(n_hf(atm%m%np, states_batch_size(atm%states)))
      allocate(qn(states_batch_size(atm%states)))

      !Compute new potential
      call states_batch_calc_hf_terms(atm%states, atm%m, atm%z, qn, v_hf, n_hf)

      !Update potential
      if (present(potential)) then
        call potential_update_vhf(potential, atm%m, states_batch_size(atm%states), v_hf, n_hf)
      else        
        call potential_update_vhf(atm%potential, atm%m, states_batch_size(atm%states), v_hf, n_hf)
      end if

      deallocate(v_hf, n_hf, qn)
    end select

    call pop_sub()
  end subroutine atom_update_potential

  !-----------------------------------------------------------------------
  !> Given an atomic number it returns the corresponding chemical symbol.  
  !-----------------------------------------------------------------------
  function label(z)
    real(R8), intent(in) :: z !< atomic number 
    character(3) :: label     !< chemical symbol

    character(3), dimension(112), parameter :: table= &
         (/'H  ','He ','Li ','Be ','B  ','C  ','N  ','O  ','F  ','Ne ','Na ', &
           'Mg ','Al ','Si ','P  ','S  ','Cl ','Ar ','K  ','Ca ','Sc ','Ti ', &
           'V  ','Cr ','Mn ','Fe ','Co ','Ni ','Cu ','Zn ','Ga ','Ge ','As ', &
           'Se ','Br ','Kr ','Rb ','Sr ','Y  ','Zr ','Nb ','Mo ','Tc ','Ru ', &
           'Rh ','Pd ','Ag ','Cd ','In ','Sn ','Sb ','Te ','I  ','Xe ','Cs ', &
           'Ba ','La ','Ce ','Pr ','Nd ','Pm ','Sm ','Eu ','Gd ','Tb ','Dy ', &
           'Ho ','Er ','Tm ','Yb ','Lu ','Hf ','Ta ','W  ','Re ','Os ','Ir ', &
           'Pt ','Au ','Hg ','Tl ','Pb ','Bi ','Po ','At ','Rn ','Fr ','Ra ', &
           'Ac ','Th ','Pa ','U  ','Np ','Pu ','Am ','Cm ','Bk ','Cf ','Es ', &
           'Fm ','Md ','No ','Lr ','Rf ','Db ','Sg ','Bh ','Hs ','Mt ','Uun', &
           'Uuu','Uub'/)

    if (int(z) > 112) then
      label='unk'
    else
      label=table(int(z))
    end if

  end function label

  !-----------------------------------------------------------------------
  !> Writes the density, the wavefunctions and the potential to the dir    
  !> directory.                                                            
  !-----------------------------------------------------------------------
  subroutine atom_output(atm, dir)
    type(atom_t),     intent(in) :: atm !< atom information
    character(len=*), intent(in) :: dir !< name of directory where to write the files

    integer :: i
    
    call push_sub("atom_output") 

    if (atm%type == ATOM_AE .or. atm%type == ATOM_PS) then
      call states_batch_output_density(atm%states, atm%nspin, atm%m, dir)
      do i = 1, states_batch_size(atm%states)
        call state_output_wf(states_batch_get(atm%states, i), atm%m, dir)
      end do
      call xc_output(atm%xc_model, atm%m, atm%states, atm%nspin, dir)
      call hartree_output(atm%m, states_batch_charge_density(atm%states, atm%m), &
           states_batch_charge(atm%states), dir)
    end if

    call potential_output(atm%potential, dir)

    call pop_sub()
  end subroutine atom_output

#include "atom_ae_inc.F90"
#include "atom_ps_inc.F90"

end module atom_m
