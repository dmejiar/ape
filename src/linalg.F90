!! Copyright (C) 2004-2010 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module linalg_m
  use global_m
  use gsl_interface_m
  use messages_m
  implicit none


                    !---Public/Private Statements---!

  private
  public :: matrix_invert, solve_linear_system, gsl_vector_init

contains

  !-----------------------------------------------------------------------
  !> Computes the inverse of a square matrix from its LU decomposition    
  !> using the GSL library.                                               
  !-----------------------------------------------------------------------
  subroutine matrix_invert(n, matrix)
    integer,  intent(in)    :: n            !< matrix dimension
    real(R8), intent(inout) :: matrix(n, n) !< matrix to be inverted on input and inverted matrix on output

    integer(POINTER_SIZE) :: m, p, inv
    integer :: ierr, i, j, signum

    call gsl_matrix_init(n, n, matrix, m)
    call gsl_permutation_alloc(n, p)
    call gsl_matrix_alloc(n, n, inv)

    ierr = gsl_linalg_lu_decomp(m, p, signum)
    if (ierr /= 0) then
      write(message(1),'(A,I4)') 'In matrix_invert, gsl_linalg_lu_decomp returned error code:', ierr
      call write_fatal(1)
    end if
    ierr = gsl_linalg_lu_invert(m,p,inv)
    if (ierr /= 0) then
      write(message(1),'(A,I4)') 'In matrix_invert, gsl_linalg_lu_invert returned error code:', ierr
      call write_fatal(1)
    end if

    forall (i=0:n-1,j=0:n-1) matrix(i+1,j+1) = gsl_matrix_get(inv, i, j)

    call gsl_matrix_free(m)
    call gsl_permutation_free(p)
    call gsl_matrix_free(inv)

  end subroutine matrix_invert

  !-----------------------------------------------------------------------
  !> Solves a linear system of equations of the form Ax = b using the GSL 
  !> library.                                                             
  !-----------------------------------------------------------------------
  subroutine solve_linear_system(n, matrix_a, vector_b, vector_x)
    integer,  intent(in)  :: n              !< dimension of the system
    real(R8), intent(in)  :: matrix_a(n, n) !< left hand side of the equations
    real(R8), intent(in)  :: vector_b(n)    !< righ hand side of the equations
    real(R8), intent(out) :: vector_x(n)    !< solutions of the equations

    integer(POINTER_SIZE) :: a, b, x, p
    integer :: ierr, i, signum

    call gsl_matrix_init(n, n, matrix_a, a)
    call gsl_permutation_alloc(n, p)
    call gsl_vector_init(n, vector_b, b)
    call gsl_vector_alloc(n, x)

    ierr = gsl_linalg_lu_decomp(a, p, signum)
    if (ierr /= 0) then
      write(message(1),'(A,I4)') 'In solve_linear_system, gsl_linalg_lu_decomp returned error code:', ierr
      call write_fatal(1)
    end if
    ierr=gsl_linalg_lu_solve(a, p, b, x)
    if (ierr /= 0) then
      write(message(1),'(A,I4)') 'In solve_linear_system, gsl_linalg_lu_solve returned error code:', ierr
      call write_fatal(1)
    end if

    forall (i=0:n-1) vector_x(i+1) = gsl_vector_get(x, i)
       
    call gsl_matrix_free(a)
    call gsl_permutation_free(p)
    call gsl_vector_free(x)
    call gsl_vector_free(b)

  end subroutine solve_linear_system

  !-----------------------------------------------------------------------
  !> Initializes a GSL vector object setting its elements using the values
  !> stored in an array.                                                  
  !-----------------------------------------------------------------------
  subroutine gsl_vector_init(n, vector, v)
    integer,               intent(in)    :: n         !< size of the vector
    real(R8),              intent(in)    :: vector(n) !< array of vector elements
    integer(POINTER_SIZE), intent(inout) :: v         !< pointer to the GSL vector object

    integer :: i

    v = 0
    call gsl_vector_alloc(n, v)
    do i = 0, n - 1
      call gsl_vector_set(v, i, vector(i+1))
    end do

  end subroutine gsl_vector_init

  !-----------------------------------------------------------------------
  !> Initializes a GSL matrix object setting its elements using the values
  !> stored in an array.                                                  
  !-----------------------------------------------------------------------
  subroutine gsl_matrix_init(n1, n2, matrix, m)
    integer,               intent(in)    :: n1             !< number of rows
    integer,               intent(in)    :: n2             !< number of rows
    real(R8),              intent(in)    :: matrix(n1, n2) !< array of matrix elements
    integer(POINTER_SIZE), intent(inout) :: m              !< pointer to the GSL matrix object

    integer :: i, j

    m = 0 
    call gsl_matrix_alloc(n1, n2, m)
    do i = 0, n1 - 1
      do j = 0, n2 - 1
        call gsl_matrix_set(m, i, j, matrix(i+1, j+1))
      end do
    end do

  end subroutine gsl_matrix_init

end module linalg_m
