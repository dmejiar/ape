!! Copyright (C) 2004-2011 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module multireference_equations_m
  use global_m
  use messages_m
  use mesh_m
  use quantum_numbers_m
  use potentials_m
  use linalg_m
  use wave_equations_m
  implicit none


                    !---Global Variables---!

  logical            :: rel
  integer            :: wave_eq, wf_dim
  real(R8)           :: rc, ev0, ev1, rhs(9)
  type(mesh_t)       :: m, mrc
  type(potential_t)  :: ae_potential
  type(integrator_t) :: integrator
  type(qn_t)         :: qn0, qn1

  !Stuff to compute the polynomial
  real(R8) :: p0_rc(8), p1_rc(8), p2_rc(8), p3_rc(8), p4_rc(8)

  real(R8), allocatable :: p0_r(:,:), p1_r(:,:), p2_r(:,:)

  !Work arrays
  real(R8), allocatable :: ps_wf0(:,:), ps_wf0p(:,:), ps_wf1(:,:), ps_wf1p(:,:), ps_v(:)


                    !---Public/Private Statements---!

  private
  public :: mrpp_equations_init, &
            mrpp_equations, &
            mrpp_equations_end, &
            mrpp_ps_wavefunctions, &
            mrpp_solve_linear_system, &
            mrpp_equations_write_info


contains

  !-----------------------------------------------------------------------
  !> Initializes global data needed to solve the MRPP set of non-linear   
  !> equations.                                                           
  !-----------------------------------------------------------------------
  subroutine mrpp_equations_init(mp, mrcp, relp, wave_eqp, integratorp, &
       ae_potentialp, rcp, qn0p, qn1p, ev0p, ev1p, rhsp)
    type(mesh_t),       intent(in) :: mp            !< mesh
    type(mesh_t),       intent(in) :: mrcp          !< truncated mesh
    logical,            intent(in) :: relp          !< if true, use the relativistic extension to the scheme
    integer,            intent(in) :: wave_eqp      !< wave equation to use
    type(integrator_t), intent(in) :: integratorp   !< integrator object
    type(potential_t),  intent(in) :: ae_potentialp !< all-electron potential
    real(R8),           intent(in) :: rcp           !< cutoff radius
    type(qn_t),         intent(in) :: qn0p          !< quantum numbers of semi-core state
    type(qn_t),         intent(in) :: qn1p          !< quantum numbers of valence state
    real(R8),           intent(in) :: ev0p          !< eigenvalue of semi-core state
    real(R8),           intent(in) :: ev1p          !< eigenvalue of valence state
    real(R8),           intent(in) :: rhsp(9)       !< right-hand side of equations

    integer  :: i
    real(R8), parameter :: FACTORS(8) = (/M_TWO, M_FOUR, M_SIX, M_EIGHT, M_TEN, &
                                     M_TWELVE, M_FOURTEEN, M_SIXTEEN/)

    !
    call mesh_null(m)
    call mesh_null(mrc)
    call potential_null(ae_potential)

    !
    m = mp; mrc = mrcp
    rel = relp
    wave_eq = wave_eqp
    integrator = integratorp
    ae_potential = ae_potentialp
    rc = rcp
    qn0 = qn0p; qn1 = qn1p
    ev0 = ev0p; ev1 = ev1p
    rhs = rhsp

    !Initialize stuff to compute the polynomial
    allocate(p0_r(mrc%np, 9), p1_r(mrc%np, 9), p2_r(mrc%np, 9))

    p0_r(:, 1) = mrc%r**2;  p1_r(:, 1) = mrc%r;     p2_r(:, 1) = M_ONE;
    p0_r(:, 2) = mrc%r**4;  p1_r(:, 2) = mrc%r**3;  p2_r(:, 2) = mrc%r**2;
    p0_r(:, 3) = mrc%r**6;  p1_r(:, 3) = mrc%r**5;  p2_r(:, 3) = mrc%r**4;
    p0_r(:, 4) = mrc%r**8;  p1_r(:, 4) = mrc%r**7;  p2_r(:, 4) = mrc%r**6;
    p0_r(:, 5) = mrc%r**10; p1_r(:, 5) = mrc%r**9;  p2_r(:, 5) = mrc%r**8;
    p0_r(:, 6) = mrc%r**12; p1_r(:, 6) = mrc%r**11; p2_r(:, 6) = mrc%r**10;
    p0_r(:, 7) = mrc%r**14; p1_r(:, 7) = mrc%r**13; p2_r(:, 7) = mrc%r**12
    p0_r(:, 8) = mrc%r**16; p1_r(:, 8) = mrc%r**15; p2_r(:, 8) = mrc%r**14

    do i = 1, 8
      p1_r(:, i) = p1_r(:, i)*FACTORS(i)
      p2_r(:, i) = p2_r(:, i)*(FACTORS(i) - M_ONE)*FACTORS(i)
    end do

    p0_rc(1) = rc**2;  p1_rc(1) = rc;     p2_rc(1) = M_ONE;   p3_rc(1) = M_ZERO;  p4_rc(1) = M_ZERO
    p0_rc(2) = rc**4;  p1_rc(2) = rc**3;  p2_rc(2) = rc**2;  p3_rc(2) = rc;     p4_rc(2) = M_ONE
    p0_rc(3) = rc**6;  p1_rc(3) = rc**5;  p2_rc(3) = rc**4;  p3_rc(3) = rc**3;  p4_rc(3) = rc**2
    p0_rc(4) = rc**8;  p1_rc(4) = rc**7;  p2_rc(4) = rc**6;  p3_rc(4) = rc**5;  p4_rc(4) = rc**4
    p0_rc(5) = rc**10; p1_rc(5) = rc**9;  p2_rc(5) = rc**8;  p3_rc(5) = rc**7;  p4_rc(5) = rc**6
    p0_rc(6) = rc**12; p1_rc(6) = rc**11; p2_rc(6) = rc**10; p3_rc(6) = rc**9;  p4_rc(6) = rc**8
    p0_rc(7) = rc**14; p1_rc(7) = rc**13; p2_rc(7) = rc**12; p3_rc(7) = rc**11; p4_rc(7) = rc**10
    p0_rc(8) = rc**16; p1_rc(8) = rc**15; p2_rc(8) = rc**14; p3_rc(8) = rc**13; p4_rc(8) = rc**12

    p1_rc = p1_rc*FACTORS
    p2_rc = p2_rc*(FACTORS - M_ONE)*FACTORS
    p3_rc = p3_rc*(FACTORS - M_TWO)*(FACTORS - M_ONE)*FACTORS
    p4_rc = p4_rc*(FACTORS - M_THREE)*(FACTORS - M_TWO)*(FACTORS - M_ONE)*FACTORS

    !
    wf_dim = qn_wf_dim(qn0)
    allocate(ps_wf0(mrc%np, wf_dim), ps_wf0p(mrc%np, wf_dim))
    allocate(ps_wf1(mrc%np, wf_dim), ps_wf1p(mrc%np, wf_dim))
    allocate(ps_v(mrc%np))

  end subroutine mrpp_equations_init

  !-----------------------------------------------------------------------
  !> For a given set of coefficients x, returns the difference f_x between
  !> the left-hand side and the right-hand side of the equations.         
  !-----------------------------------------------------------------------
  subroutine mrpp_equations(n, x, f_x)
    integer,  intent(in)  :: n
    real(R8), intent(in)  :: x(n)
    real(R8), intent(out) :: f_x(n)

    real(R8) :: xx(9), ldd

    xx = M_ZERO
    xx(1) = x(1)
    xx(7:9) = x(2:4)

    !Solve linear system
    call mrpp_solve_linear_system(xx)

    !Get the pseudopotential and the wavefunctions
    call mrpp_ps_wavefunctions(xx, ps_wf0, ps_wf0p, ps_v, ldd, ps_wf1, ps_wf1p)

    !Return the values we want to be zero
    f_x(1) = (xx(2)**2 + (M_TWO*qn0%l + M_FIVE)*xx(3) - rhs(6))
    if (rel) then
      f_x(2) = mesh_integrate(mrc, sum(ps_wf0**2, dim=2), b=rc) - rhs(7)
    else
      f_x(2) = mesh_integrate(mrc, ps_wf0(:,1)**2, b=rc) - rhs(7)
    end if
    f_x(3) = ps_wf1(mrc%np,1)*rc - rhs(8)
    f_x(4) = ldd - rhs(9)

  end subroutine mrpp_equations

  !-----------------------------------------------------------------------
  !> Solve the linear part of the system of equations.                    
  !-----------------------------------------------------------------------
  subroutine mrpp_solve_linear_system(c)
    real(R8), intent(inout) :: c(9)

    real(R8) :: lsrhs(5), r(5,5)

    lsrhs(1) = rhs(1) - sum(p0_rc(6:8)*c(7:9)) - c(1)
    lsrhs(2) = rhs(2) - sum(p1_rc(6:8)*c(7:9))
    lsrhs(3) = rhs(3) - sum(p2_rc(6:8)*c(7:9))
    lsrhs(4) = rhs(4) - sum(p3_rc(6:8)*c(7:9))
    lsrhs(5) = rhs(5) - sum(p4_rc(6:8)*c(7:9))
    r(1,:) = p0_rc(1:5)
    r(2,:) = p1_rc(1:5)
    r(3,:) = p2_rc(1:5)
    r(4,:) = p3_rc(1:5)
    r(5,:) = p4_rc(1:5)
    call solve_linear_system(5, r, lsrhs, c(2:6))

  end subroutine mrpp_solve_linear_system

  !-----------------------------------------------------------------------
  !> For a given set of coefficients c, returns the corresponding pseudo  
  !> wave-functions and pseudo-potential.                                 
  !-----------------------------------------------------------------------
  subroutine mrpp_ps_wavefunctions(c, wf0, wf0p, pot, ldd, wf1, wf1p)
    real(R8), intent(in)  :: c(9)
    real(R8), intent(out) :: wf0(mrc%np, wf_dim), wf0p(mrc%np, wf_dim)
    real(R8), intent(out) :: pot(mrc%np)
    real(R8), intent(out) :: wf1(mrc%np, wf_dim), wf1p(mrc%np, wf_dim)
    real(R8), intent(out) :: ldd

    integer :: i, nnodes
    real(R8), allocatable :: p(:), pp(:), ppp(:), vpp(:), wf(:,:), wfp(:,:)
    type(potential_t) :: ps_potential

    call potential_null(ps_potential)

    !Compute the polynomial and its derivatives
    allocate(p(mrc%np), pp(mrc%np), ppp(mrc%np))
    p   = c(1)
    pp  = M_ZERO
    ppp = M_ZERO
    do i = 1, 8
      p   = p   + c(1+i)*p0_r(:,i)
      pp  = pp  + c(1+i)*p1_r(:,i)
      ppp = ppp + c(1+i)*p2_r(:,i)
    end do

    pot = ev0 + (real(qn0%l,r8) + M_ONE)/mrc%r*pp + (ppp + pp**2)/M_TWO
    wf0(:,1) = mrc%r**qn0%l*exp(p)
    if (qn0%l == 0) then
      wf0p(:,1) = pp*exp(p)
    else
      wf0p(:,1) = real(qn0%l,R8)*mrc%r**(qn0%l-1)*exp(p) + mrc%r**qn0%l*pp*exp(p)
    end if


    if (rel) then
      !Relativistic correction to the pseudopotential
      call potential_init(ps_potential, mrc, pot)
      do i = 1, mrc%np
        pot(i) = pot(i) + (pot(i) - ev0)**2/M_TWO/M_C2 + dvdr(ps_potential, mrc%r(i), qn0)/M_FOUR/M_C2*(wf0p(i,1)/wf0(i,1)&
                 + (qn0%k + M_ONE)/mrc%r(i))
      end do
      call potential_end(ps_potential)

      !Minor component
      wf0(:,2) = M_C*((real(qn0%l,R8) + M_ONE + qn0%k)/mrc%r + pp)*wf0(:,1)/(M_TWO*M_C2 - pot + ev0)
      wf0p(:,2) = mesh_derivative(m, wf0(:,2))
    else
      wf0(:,2:wf_dim) = M_ZERO
      wf0p(:,2:wf_dim) = M_ZERO
    end if
    deallocate(p, pp, ppp)

    !
    allocate(vpp(m%np), wf(m%np, wf_dim), wfp(m%np, wf_dim))
    vpp(1:mrc%np) = pot
    do i = mrc%np+1, m%np
      vpp(i) = v(ae_potential, m%r(i), qn1)
    end do
    call potential_init(ps_potential, m, vpp)
    if (rel) then
      call wavefunctions(qn1, ev1, DIRAC, m, ps_potential, integrator, wf, wfp)
      ldd = wave_equation_ld_diff(qn1, ev1, DIRAC, ps_potential, integrator, nnodes)
    else
      call wavefunctions(qn1, ev1, wave_eq, m, ps_potential, integrator, wf, wfp)
      ldd = wave_equation_ld_diff(qn1, ev1, wave_eq, ps_potential, integrator, nnodes)
      wf(:,2:wf_dim) = M_ZERO
      wfp(:,2:wf_dim) = M_ZERO
    end if
    do i = 1, wf_dim
      call mesh_transfer(m, wf(:,i), mrc, wf1(:,i))
      call mesh_transfer(m, wfp(:,i), mrc, wf1p(:,i))
    end do
    ldd = ldd*10.0
    call potential_end(ps_potential)
    deallocate(vpp, wf, wfp)

  end subroutine mrpp_ps_wavefunctions

  !-----------------------------------------------------------------------
  !> Writes to the screen some information about the multiroot solving.   
  !-----------------------------------------------------------------------
  subroutine mrpp_equations_write_info(iter, n, c, f)
    integer,  intent(in) :: iter, n
    real(R8), intent(in) :: c(n), f(n)

    real(R8) :: cc(9)

    ASSERT(n == 4)

    cc = M_ZERO
    cc(1) = c(1)
    cc(7:9) = c(2:4)
    call mrpp_solve_linear_system(cc)

    write(message(1), '(6x,"Iteration number: ",I3)') iter
    write(message(2), '(6x,"Coefficients:")')
    write(message(3), '(8x,"c0  =",1x,es16.9e2,3x,"c2  =",1x,es16.9e2)') cc(1), cc(2)
    write(message(4), '(8x,"c4  =",1x,es16.9e2,3x,"c6  =",1x,es16.9e2)') cc(3), cc(4)
    write(message(5), '(8x,"c8  =",1x,es16.9e2,3x,"c10 =",1x,es16.9e2)') cc(5), cc(6)
    write(message(6), '(8x,"c12 =",1x,es16.9e2,3x,"c14 =",1x,es16.9e2)') cc(7), cc(8)
    write(message(7), '(8x,"c16 =",1x,es16.9e2)') cc(9)
    write(message(8), '(6x,"Residues:")')
    write(message(9), '(8X,"c2**2 + c4(2l+5)                  =",1x,ES16.9e2)') f(1)
    write(message(10),'(8X,"norm_sc[AE] - norm_sc[PP]         =",1x,ES16.9e2)') f(2)
    write(message(11),'(8X,"R(rc)[AE] - R(rc)[PP]             =",1x,ES16.9e2)') f(3)
    write(message(12),'(8X,"R''/R(rc)[AE] - R''/R(rc)[PP]       =",1x,ES16.9e2)') f(4)
    message(13) = ""
    call write_info(13, 30)

  end subroutine mrpp_equations_write_info

  !-----------------------------------------------------------------------
  !> Frees all the memory associated to the global data needed to solve   
  !> the MRPP set of non-linear equations.                                
  !-----------------------------------------------------------------------
  subroutine mrpp_equations_end()
    
    deallocate(p0_r, p1_r, p2_r)
    deallocate(ps_wf0, ps_wf0p, ps_wf1, ps_wf1p, ps_v)
    call mesh_end(m)
    call mesh_end(mrc)
    call potential_end(ae_potential)

  end subroutine mrpp_equations_end

end module multireference_equations_m
