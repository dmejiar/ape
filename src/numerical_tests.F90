!! Copyright (C) 2011 M. Oliveira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module numerical_tests_m
  use iso_c_binding
  use global_m
  use oct_parser_m
  use messages_m
  use io_m
  use output_m
  use utilities_m
  use mesh_m
  use hartree_m
  implicit none


                    !---Derived Data Types---!

  type function_t
    private
    integer  :: type
    integer  :: n_params
    real(R8) :: params(3)
    character(20) :: name
  end type function_t


                     !---Global Variables---!

  ! Functions type
  integer, parameter :: GAUSSIAN    = 1, & ! A*exp(-(r-B)**2/(2*C))
                        INVERSE     = 2, & ! A/(r-B)
                        EXPONENTIAL = 3    ! A*exp(B*(r-C))

  ! Type of tests
  integer, parameter :: TEST_DERIVATIVES = 1, &
                        TEST_INTEGRATION = 2, &
                        TEST_HARTREE     = 4


                    !---Public/Private Statements---!

  private
  public :: numerical_tests_run, &
            GAUSSIAN, INVERSE, EXPONENTIAL, &
            TEST_DERIVATIVES, TEST_INTEGRATION, TEST_HARTREE

contains

  !-----------------------------------------------------------------------
  !> Performs several tests to check the numerical accuracy of the code.  
  !-----------------------------------------------------------------------
  subroutine numerical_tests_run()
    type(mesh_t) :: m

    integer :: i, p, n_funcs, tests, ierr
    type(c_ptr) :: blk
    type(function_t), allocatable :: functions(:)

    call push_sub("numerical_tests_run")
    
    !Initialize mesh mesh
    call mesh_null(m)
    call mesh_init_from_input(50.0_r8, m)
    call mesh_output_params(m)
    call mesh_output_params(m,unit=info_unit("nt"))

    !Read functions to use during the test
    if (oct_parse_f90_block("TestFunctions", blk) == 0) then
      n_funcs = oct_parse_f90_block_n(blk)
    else
      n_funcs = 0
    end if
    allocate(functions(n_funcs))
    do i = 1, n_funcs
      !Check if we have at least the name and type of the function
      if (oct_parse_f90_block_cols(blk, i-1) < 2) then
        write(message(1),'("Wrong number of columns in line ",I1," of the TestFunctions block")') i
        call write_fatal(1)
      end if

      !Read name and type of the function and set the number of parameters
      ierr = oct_parse_f90_block_string(blk, i-1, 0, functions(i)%name)
      if (ierr /= 0) then
        write(message(1),'("Unable to read a string from line ",I2," and column ",I2,", of the TestFunctions block.")') i, 1
        call write_fatal(1)
      end if
      ierr = oct_parse_f90_block_int(blk, i-1, 1, functions(i)%type)
      if (ierr /= 0) then
        write(message(1),'("Unable to read an integer from line ",I2," and column ",I2,", of the TestFunctions block.")') i, 2
        call write_fatal(1)
      end if
      select case (functions(i)%type)
      case (GAUSSIAN)
        functions(i)%n_params = 3
      case (INVERSE)
        functions(i)%n_params = 2
      case (EXPONENTIAL)
        functions(i)%n_params = 3
      case default
        write(message(1),'("Unknown function type in line ",I1," of the TestFunctions block")') i
        call write_fatal(1)
      end select

      !Read the parameters
      if (oct_parse_f90_block_cols(blk, i-1) < 2+functions(i)%n_params) then
        write(message(1),'("Not enough parameters in line ",I1," of the TestFunctions block")') i
        call write_fatal(1)
      end if
      do p = 1, functions(i)%n_params
        ierr = oct_parse_f90_block_double(blk, i-1, 1+p, functions(i)%params(p))
        if (ierr /= 0) then
          write(message(1),'("Unable to read a double from line ",I2," and column ",I2,", of the TestFunctions block.")') i, p
          call write_fatal(1)
        end if
      end do
      
    end do
    call oct_parse_f90_block_end(blk)

    message(1) = ""
    message(2) = ""
    message(3) = str_center("-- Numerical Tests --", 70)
    call write_info(3)

    !What tests should we perform?
    tests = oct_parse_f90_int("NumericalTests", TEST_DERIVATIVES + TEST_INTEGRATION + TEST_HARTREE)

    if (iand(tests, TEST_DERIVATIVES) /= 0) then
      !Test derivatives
      message(1) = ""
      message(2) = "Testing Derivatives:"
      call write_info(2)
      call write_info(2, unit=info_unit("nt"))
      do i = 1, n_funcs
        call test_function_derivatives(m, functions(i))
      end do
    end if

    if (iand(tests, TEST_INTEGRATION) /= 0) then
      !Test integration
      message(1) = ""
      message(2) = "Testing Integration:"
      call write_info(2)
      call write_info(2, unit=info_unit("nt"))
      do i = 1, n_funcs
        if (functions(i)%type == GAUSSIAN) then
          write(message(1),'("  Skipping function ",A)') trim(functions(i)%name)
          message(2) = ""
          call write_info(2)
          cycle
        end if
        call test_function_integration(m, functions(i), m%r(1), m%r(m%np))
      end do
    end if

    if (iand(tests, TEST_HARTREE) /= 0) then
      !Test Hartree potential
      message(1) = ""
      message(2) = "Testing Hartree Potential:"
      call write_info(2)
      call write_info(2, unit=info_unit("nt"))
      do i = 1, n_funcs
        if (functions(i)%type == GAUSSIAN .or. functions(i)%type == INVERSE) then
          write(message(1),'("  Skipping function ",A)') trim(functions(i)%name)
          message(2) = ""
          call write_info(2)
          cycle
        end if
        call test_function_hartree_potential(m, functions(i))
      end do
    end if

    !Free memory
    deallocate(functions)
    call mesh_end(m)

    call pop_sub()
  end subroutine numerical_tests_run

  !-----------------------------------------------------------------------
  !> Test derivatives.
  !-----------------------------------------------------------------------
  subroutine test_function_derivatives(m, myfunc)
    type(mesh_t),     intent(in) :: m
    type(function_t), intent(in) :: myfunc

    integer :: i, unit
    real(R8), allocatable :: f(:), fp(:), fpp(:), l(:), num_fp(:), num_fpp(:), num_l(:)
    character(len=30) :: dir

    call push_sub("test_function_derivatives")

    !Print information about function
    message(1) = ""
    call write_info(1)
    call write_info(1, unit=info_unit("nt"))
    call function_output_info(myfunc)
    call function_output_info(myfunc, unit=info_unit("nt"))

    !Compute derivatives
    allocate(f(m%np), fp(m%np), fpp(m%np), l(m%np))
    allocate(num_fp(m%np), num_fpp(m%np), num_l(m%np))

    call function_eval(myfunc, m, f)
    call function_eval_derivs(myfunc, m, fp, fpp, l)
    num_fp = mesh_derivative(m, f)
    num_fpp = mesh_derivative2(m, f)
    num_l = mesh_laplacian(m, f)

    !Statistics
    write(message(1),  '(4X,"Maximum error:")')
    write(message(2),  '(16X,"r",8X,"Analitic",5X,"Computed",4X,"Difference")')
    write(message(3),  '(6X,"f''",2X,A)') trim(get_max_error(m, fp, num_fp))
    write(message(4),  '(6X,"f''''",1X,A)') trim(get_max_error(m, fpp, num_fpp))
    write(message(5),  '(6X,"lapl",A)') trim(get_max_error(m, l, num_l))
    write(message(6),  '(4X,"Minimum error:")')
    write(message(7),  '(16X,"r",8X,"Analitic",5X,"Computed",4X,"Difference")')
    write(message(8),  '(6X,"f''",2X,A)') trim(get_min_error(m, fp, num_fp))
    write(message(9),  '(6X,"f''''",1X,A)') trim(get_min_error(m, fpp, num_fpp))
    write(message(10), '(6X,"lapl",A)') trim(get_min_error(m, l, num_l))
    write(message(11), '(4X,"Average error: ")') 
    write(message(12), '(6X,"f''",2X,A)') trim(get_avg_error(m, fp, num_fp))
    write(message(13), '(6X,"f''''",1X,A)') trim(get_avg_error(m, fpp, num_fpp))
    write(message(14), '(6X,"lapl",A)') trim(get_avg_error(m, l, num_l))
    call write_info(14)
    call write_info(14, unit=info_unit("nt"))

    !Output to files
    write(dir, '("nt/",A)') trim(myfunc%name)
    call sys_mkdir(dir)

    call io_open(unit, file=trim(dir)//"/derivs")

    do i = 1, m%np
      write(unit, '(8(2X,ES16.8E3))') m%r(i), f(i), fp(i), num_fp(i), fpp(i), num_fpp(i), l(i), num_l(i)
    end do

    close(unit)

    !Free memory
    deallocate(f, fp, fpp, l, num_fp, num_fpp, num_l)

    call pop_sub()
  end subroutine test_function_derivatives

  !-----------------------------------------------------------------------
  !> Test the calculation of the hartree potential.                       
  !-----------------------------------------------------------------------
  subroutine test_function_hartree_potential(m, myfunc)
    type(mesh_t),     intent(in) :: m
    type(function_t), intent(in) :: myfunc

    integer :: i, unit
    real(R8), allocatable :: f(:), vh(:), num_vh(:,:)
    character(len=30) :: dir

    call push_sub("test_function_hartree_potential")

    !Print information about function
    message(1) = ""
    call write_info(1)
    call write_info(1, unit=info_unit("nt"))
    call function_output_info(myfunc)
    call function_output_info(myfunc, unit=info_unit("nt"))

    !Compute derivatives
    allocate(f(m%np), vh(m%np), num_vh(m%np, 1))

    call function_eval(myfunc, m, f)
    call function_hartree_potential(myfunc, m, vh)
    call hartree_potential(m, f, M_ZERO, vh=num_vh)

    !Statistics
    write(message(1), '(4X,"Maximum error:")')
    write(message(2), '(10X,"r",8X,"Analitic",5X,"Computed",4X,"Difference")')
    write(message(3), '(4X,A)') trim(get_max_error(m, vh, num_vh(:,1)))
    write(message(4), '(4X,"Minimum error:")')
    write(message(5), '(10X,"r",8X,"Analitic",5X,"Computed",4X,"Difference")')
    write(message(6), '(4X,A)') trim(get_min_error(m, vh, num_vh(:,1)))
    write(message(7), '(4X,"Average error: ")') 
    write(message(8), '(4X,A)') trim(get_avg_error(m, vh, num_vh(:,1)))
    call write_info(8)
    call write_info(8, unit=info_unit("nt"))

    !Output to files
    write(dir, '("nt/",A)') trim(myfunc%name)
    call sys_mkdir(dir)

    call io_open(unit, file=trim(dir)//"/vh")

    do i = 1, m%np
      write(unit, '(4(2X,ES16.8E3))') m%r(i), f(i), vh(i), num_vh(i,1)
    end do

    close(unit)

    !Free memory
    deallocate(f, vh, num_vh)

    call pop_sub()
  end subroutine test_function_hartree_potential

  !-----------------------------------------------------------------------
  !> Test numerical integration.                                          
  !-----------------------------------------------------------------------
  subroutine test_function_integration(m, myfunc, a, b)
    type(mesh_t),     intent(in) :: m
    type(function_t), intent(in) :: myfunc
    real(r8),         intent(in) :: a, b

    real(R8) :: int, num_int
    real(R8), allocatable :: f(:)

    call push_sub("test_function_integration")

    !Print information about function
    message(1) = ""
    call write_info(1)
    call write_info(1, unit=info_unit("nt"))
    call function_output_info(myfunc)
    call function_output_info(myfunc, unit=info_unit("nt"))

    allocate(f(m%np))
    call function_eval(myfunc, m, f)
    int = function_integ(myfunc, a, b)
    num_int = mesh_integrate(m, f, a=a, b=b)

    write(message(1), '(12X,"Interval",10X,"Analitic",5X,"Computed",4X,"Difference")')
    write(message(2), '(4X,2(1X,ES10.3E2),3(2X,ES11.3E3))') a, b, int, num_int, abs(int - num_int)
    call write_info(2)
    call write_info(2, unit=info_unit("nt"))

    !Free memory
    deallocate(f)

    call pop_sub()
  end subroutine test_function_integration

  !-----------------------------------------------------------------------
  !> Compare two functions and return the maximum error, the position at  
  !> which this error occurs, and the values of the functions at that     
  !> point in a nicely formated string.                                   
  !-----------------------------------------------------------------------
  function get_max_error(m, f1, f2)
    type(mesh_t), intent(in)  :: m
    real(R8),     intent(in) :: f1(m%np)
    real(R8),     intent(in) :: f2(m%np)
    character(len=80) :: get_max_error

    integer, target :: pos(1)
    integer, pointer :: i

    i => pos(1)
    pos = maxloc(abs(f1 - f2))
    write(get_max_error, '(2X,ES9.2E2,4(2X,ES11.3E3))')  &
         m%r(i), f1(i), f2(i), abs(f1(i) - f2(i))

  end function get_max_error

  !-----------------------------------------------------------------------
  !> Compare two functions and return the minimum error, the position at  
  !> which this error occurs, and the values of the functions at that     
  !> point in a nicely formated string.                                   
  !-----------------------------------------------------------------------
  function get_min_error(m, f1, f2)
    type(mesh_t), intent(in)  :: m
    real(R8),     intent(in) :: f1(m%np), f2(m%np)
    character(len=80) :: get_min_error

    integer, target :: pos(1)
    integer, pointer :: i

    i => pos(1)
    pos = minloc(abs(f1 - f2))
    write(get_min_error, '(2X,ES9.2E2,4(2X,ES11.3E3))')  &
         m%r(i), f1(i), f2(i), abs(f1(i) - f2(i))

  end function get_min_error

  !-----------------------------------------------------------------------
  !> Compare two functions and return the average error in a nicely       
  !> formated string.                                                     
  !-----------------------------------------------------------------------
  function get_avg_error(m, f1, f2)
    type(mesh_t), intent(in)  :: m
    real(R8),     intent(in) :: f1(m%np), f2(m%np)
    character(len=80) :: get_avg_error

    write(get_avg_error, '(2X,ES11.3E3)') sum(abs(f1 - f2))/m%np
    
  end function get_avg_error

  !-----------------------------------------------------------------------
  !> Print information about myfunc to the screen or to unit.             
  !-----------------------------------------------------------------------
  subroutine function_output_info(myfunc, unit)
    type(function_t), intent(in) :: myfunc
    integer,          intent(in), optional :: unit

    call push_sub("function_output_info")

    write(message(1), '(2X,"Function: ",A)') trim(myfunc%name)
    select case(myfunc%type)
    case (GAUSSIAN)
      write(message(2), '(4X,"Definition: ",F6.2," * exp(-(r - ",F6.2,")**2/(2 * ",F6.2,"))")') &
                                       myfunc%params(1), myfunc%params(2), myfunc%params(3)
    case (INVERSE)
      write(message(2), '(4X,"Definition: ",F6.2," / (r - ",F6.2,")")') &
                                                         myfunc%params(1), myfunc%params(2)
    case (EXPONENTIAL)
      write(message(2), '(4X,"Definition: ",F6.2," * exp(",F6.2," * (r - ",F6.2,"))")') &
                                       myfunc%params(1), myfunc%params(2), myfunc%params(3)
    end select

    call write_info(2, unit=unit)

    call pop_sub()
  end subroutine function_output_info

  !-----------------------------------------------------------------------
  !> Evaluate the values of function myfunc at the points of mesh m.      
  !-----------------------------------------------------------------------
  subroutine function_eval(myfunc, m, f)
    type(function_t), intent(in)  :: myfunc
    type(mesh_t),     intent(in)  :: m
    real(R8),         intent(out) :: f(m%np)

    real(R8) :: A, B, C

    A = myfunc%params(1)
    B = myfunc%params(2)
    C = myfunc%params(3)

    select case(myfunc%type)
    case (GAUSSIAN)
      f = A*exp(-(m%r - B)**2/(M_TWO*C**2))

    case (INVERSE)
      f =  A/(m%r - B)

    case (EXPONENTIAL)
      f = A*exp(B*(m%r - C))

    case default
      write(message(1),'("I dont know how to evaluate function: ",A)') trim(myfunc%name)
      call write_fatal(1)
    end select

  end subroutine function_eval

  !-----------------------------------------------------------------------
  !> Evaluate the derivatives of function myfunc at the points of mesh m. 
  !-----------------------------------------------------------------------
  subroutine function_eval_derivs(myfunc, m, fp, fpp, l)
    type(function_t), intent(in)  :: myfunc    !< function
    type(mesh_t),     intent(in)  :: m         !< mesh
    real(R8),         intent(out) :: fp(m%np)  !< first derivative
    real(R8),         intent(out) :: fpp(m%np) !< second derivative
    real(R8),         intent(out) :: l(m%np)   !< laplacian (in spherical coordinates)

    real(R8) :: A, B, C
    real(R8), allocatable :: f(:), rmb(:)

    A = myfunc%params(1)
    B = myfunc%params(2)
    C = myfunc%params(3)

    select case(myfunc%type)
    case (GAUSSIAN)
      allocate(f(m%np), rmb(m%np))
      rmb = m%r - B
      f = A*exp(-rmb**2/(M_TWO*C**2))
      fp = -rmb/C**2*f
      fpp = ( (rmb/C)**2 - M_ONE)*f/C**2
      deallocate(f, rmb)

    case (INVERSE)
      fp = -A/(m%r - B)**2
      fpp = M_TWO*A/(m%r - B)**3

    case (EXPONENTIAL)
      fp = A*B*exp(B*(m%r - C))
      fpp = A*B**2*exp(B*(m%r - C))

    case default
      write(message(1),'("I dont know how to calculate the derivatives of function:",A)') trim(myfunc%name)
      call write_fatal(1)
    end select
    l = M_TWO*fp/m%r + fpp

  end subroutine function_eval_derivs

  !-----------------------------------------------------------------------
  !> Evaluates the integral of function myfunc between ra and rb (assuming
  !> spherical coordinates).                                              
  !-----------------------------------------------------------------------
  function function_integ(myfunc, ra, rb)
    type(function_t), intent(in) :: myfunc
    real(R8),         intent(in) :: ra, rb
    real(R8) :: function_integ

    real(R8) :: A, B, C, fa, fb

    A = myfunc%params(1)
    B = myfunc%params(2)
    C = myfunc%params(3)

    select case(myfunc%type)
    case (INVERSE)
      fa = A*(log(ra - B)*B**2 + (M_TWO*ra*B + ra**2)/M_TWO)
      fb = A*(log(rb - B)*B**2 + (M_TWO*rb*B + rb**2)/M_TWO)

    case (EXPONENTIAL)
      fa = A*((ra*B)**2 - M_TWO*ra + M_TWO)/B**3*exp(B*(ra - C))
      fb = A*((rb*B)**2 - M_TWO*rb + M_TWO)/B**3*exp(B*(rb - C))

    case default
      write(message(1),'("I dont know how to calculate integrals for function: ",A)') trim(myfunc%name)
      call write_fatal(1)
    end select
    function_integ = fb - fa

  end function function_integ

  !-----------------------------------------------------------------------
  !> Evaluates the hartree potential generated by function myfunc
  !> (assuming spherical coordinates).                                               
  !-----------------------------------------------------------------------
  subroutine function_hartree_potential(myfunc, m, vh)
    type(function_t), intent(in)  :: myfunc
    type(mesh_t),     intent(in)  :: m
    real(R8),         intent(out) :: vh(m%np)

    real(R8) :: A, B, C

    A = myfunc%params(1)
    B = myfunc%params(2)
    C = myfunc%params(3)

    select case(myfunc%type)
    case (EXPONENTIAL)
      vh = A/B**3*( (-B + M_TWO/m%r)*exp(B*(m%r - C)) - M_TWO/m%r*exp(-B*C))

    case default
      write(message(1),'("I dont know how to calculate the hartree potential for function: ",A)') trim(myfunc%name)
      call write_fatal(1)
    end select
    vh = M_FOUR*M_PI*vh

  end subroutine function_hartree_potential

end module numerical_tests_m
