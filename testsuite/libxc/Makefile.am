## Process this file with automake to produce Makefile.in

## Copyright (C) 2005-2011 M. Oliveira, F. Nogueira
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##

sharedir = $(pkgdatadir)/testsuite/libxc

dist_share_DATA = \
	01-H.inp \
	01-H.test \
	02-H_spin.inp \
	02-H_spin.test \
	03-He.inp \
	03-He.test \
	04-He_spin.inp \
	04-He_spin.test \
	05-Li.inp \
	05-Li.test
	000-none.inp \
	001-lda_x.inp \
	002-lda_c_wigner.inp \
	003-lda_c_rpa.inp \
	004-lda_c_hl.inp \
	005-lda_c_gl.inp \
	006-lda_c_xalpha.inp \
	007-lda_c_vwn.inp \
	008-lda_c_vwn_rpa.inp \
	009-lda_c_pz.inp \
	010-lda_c_pz_mod.inp \
	011-lda_c_ob_pz.inp \
	012-lda_c_pw.inp \
	013-lda_c_pw_mod.inp \
	014-lda_c_ob_pw.inp \
	017-lda_c_vbh.inp \
	020-lda_xc_teter93.inp \
	022-lda_c_ml1.inp \
	023-lda_c_ml2.inp \
	024-lda_c_gombas.inp \
	101-gga_x_pbe.inp \
	102-gga_x_pbe_r.inp \
	103-gga_x_b86.inp \
	104-gga_x_herman.inp \
	105-gga_x_b86_mgc.inp \
	106-gga_x_b88.inp \
	107-gga_x_g96.inp \
	108-gga_x_pw86.inp \
	109-gga_x_pw91.inp \
	110-gga_x_optx.inp \
	111-gga_x_dk87_r1.inp \
	112-gga_x_dk87_r2.inp \
	113-gga_x_lg93.inp \
	114-gga_x_ft97_a.inp \
	115-gga_x_ft97_b.inp \
	116-gga_x_pbe_sol.inp \
	117-gga_x_rpbe.inp \
	118-gga_x_wc.inp \
	119-gga_x_mpw91.inp \
	120-gga_x_am05.inp \
	121-gga_x_pbea.inp \
	122-gga_x_mpbe.inp \
	123-gga_x_xpbe.inp \
	125-gga_x_bayesian.inp \
	126-gga_x_pbe_jsjr.inp \
	130-gga_c_pbe.inp \
	131-gga_c_lyp.inp \
	132-gga_c_p86.inp \
	133-gga_c_pbe_sol.inp \
	134-gga_c_pw91.inp \
	135-gga_c_am05.inp \
	136-gga_c_xpbe.inp \
	137-gga_c_lm.inp \
	138-gga_c_pbe_jrgx.inp \
	139-gga_x_optb88_vdw.inp \
	140-gga_x_pbek1_vdw.inp \
	141-gga_x_optpbe_vdw.inp \
	142-gga_x_rge2.inp \
	143-gga_c_rge2.inp \
	144-gga_x_rpw86.inp \
	145-gga_x_kt1.inp \
	146-gga_xc_kt2.inp \
	147-gga_c_wl.inp \
	148-gga_c_wi.inp \
	149-gga_x_mb88.inp \
	160-gga_x_lb.inp \
	161-gga_xc_hcth_93.inp \
	162-gga_xc_hcth_120.inp \
	163-gga_xc_hcth_147.inp \
	164-gga_xc_hcth_407.inp \
	165-gga_xc_edf1.inp \
	166-gga_xc_xlyp.inp \
	167-gga_xc_b97.inp \
	168-gga_xc_b97_1.inp \
	169-gga_xc_b97_2.inp \
	170-gga_xc_b97_d.inp \
	171-gga_xc_b97_k.inp \
	172-gga_xc_b97_3.inp \
	173-gga_xc_pbe1w.inp \
	174-gga_xc_mpwlyp1w.inp \
	175-gga_xc_pbelyp1w.inp \
	176-gga_xc_sb98_1a.inp \
	177-gga_xc_sb98_1b.inp \
	178-gga_xc_sb98_1c.inp \
	179-gga_xc_sb98_2a.inp \
	180-gga_xc_sb98_2b.inp \
	181-gga_xc_sb98_2c.inp \
	183-gga_x_ol2.inp \
	184-gga_x_apbe.inp \
	185-gga_k_apbe.inp \
	186-gga_c_apbe.inp \
	187-gga_k_tw1.inp \
	188-gga_k_tw2.inp \
	189-g90_k_tw4.inp \
	189-gga_k_tw3.inp \
	190-gga_k_tw4.inp \
	191-gga_x_htbs.inp \
	192-gga_x_airy.inp \
	193-gga_x_lag.inp \
	201-mgga_x_lta.inp \
	202-mgga_x_tpss.inp \
	203-mgga_x_m06l.inp \
	204-mgga_x_gvt4.inp \
	205-mgga_x_tau_hcth.inp \
	205-mgga_x_tau_htch.inp \
	206-mgga_x_br89.inp \
	207-mgga_x_bj06.inp \
	208-mgga_x_tb09.inp \
	209-mgga_x_rpp10.inp \
	231-mgga_c_tpss.inp \
	232-mgga_c_vsxc.inp \
	500-gga_k_vw.inp \
	501-gga_k_ge2.inp \
	502-gga_k_golden.inp \
	503-gga_k_yt65.inp \
	504-gga_k_baltin.inp \
	505-gga_k_lieb.inp \
	506-gga_k_absr1.inp \
	507-gga_k_absr2.inp \
	508-gga_k_gr.inp \
	509-gga_k_ludena.inp \
	510-gga_k_gp85.inp \
	511-gga_k_pearson.inp \
	512-gga_k_ol1.inp \
	513-gga_k_ol2.inp \
	514-gga_k_fr_b88.inp \
	515-gga_k_fr_pw86.inp \
	516-gga_k_dk.inp \
	517-gga_k_perdew.inp \
	518-gga_k_vsk.inp \
	519-gga_k_vjks.inp \
	520-gga_k_ernzerhof.inp \
	521-gga_k_lc94.inp \
	522-gga_k_llp.inp \
	523-gga_k_thakkar.inp

CLEANFILES = *~ *.bak
